package com.karallo.drosimond;

import com.karallo.drosimond.R;
import com.karallo.drosimond.game.GameScreen;
import com.karallo.drosimond.scores.GameScores;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class MainMenu extends Activity implements OnClickListener {
	private SharedPreferences prefs_readonly = null;
	
	private Button new_game		= null;
	private Button score_chart	= null;
	private Button new_player	= null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_menu);
		
		this.new_game		= ((Button) this.findViewById(R.id.start_game));
		this.score_chart	= ((Button) this.findViewById(R.id.score_chart));
		this.new_player		= ((Button) this.findViewById(R.id.new_player));
		
		this.new_game.setOnClickListener(this);
		this.score_chart.setOnClickListener(this);
		this.new_player.setOnClickListener(this);
		
		prefs_readonly = this.getSharedPreferences("DroSimond", 0);
		Toast.makeText(this, getString(R.string.welcome_message)+" "+prefs_readonly.getString("Username", "Anonimo"), Toast.LENGTH_LONG).show();
	}

	@Override
	public void onClick(View v) {
		if (v == this.new_game) {
			Intent intent = new Intent(this, GameScreen.class);
	        this.startActivity(intent);
			
		} else if (v == this.score_chart) {
			Intent intent = new Intent(this, GameScores.class);
	        this.startActivity(intent);
			
		} else if (v == this.new_player) {
			Intent intent = new Intent(this, NewUser.class);
	        this.startActivity(intent);
			
		}
	}

}
