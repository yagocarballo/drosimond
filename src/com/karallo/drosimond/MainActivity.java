package com.karallo.drosimond;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

public class MainActivity extends Activity {
	private SharedPreferences prefs_readonly = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		prefs_readonly = this.getSharedPreferences("DroSimond", 0);
		if (prefs_readonly.getString("Username", null) == null) {
			Intent intent = new Intent(this, NewUser.class);
	        this.startActivity(intent);
	        
		} else {
			Intent intent = new Intent(this, MainMenu.class);
	        this.startActivity(intent);
		}
	}
}
