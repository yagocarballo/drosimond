package com.karallo.drosimond.game;

import java.util.Random;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import com.karallo.drosimond.MainMenu;
import com.karallo.drosimond.R;
import com.karallo.drosimond.database.Database;
import com.karallo.drosimond.scores.GameScores;

public class GameScreen extends Activity implements CTimerTask, OnClickListener {
	private SharedPreferences prefs_readonly = null;
	private final Button[] buttons = new Button[6];
	private final Random rand = new Random();
	private final GameScreen self = this;
	
	private CTimer timer = new CTimer(1000, 300, this);
	private int[] answers = null;
	
	private int level				= 1;
	private int times_counter	 	= 0;
	private int user_touch_times 	= 0;
	private int user_errors			= 0;
	
	private int last = -99;
	private int count_last = 1;
	
	/**
	 * este metodo se ejecuta al iniciar la vista/activity y crea la interfaz
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_game_screen);
		
		if (this.getIntent().getExtras() != null)
			this.level = this.getIntent().getExtras().getInt("level");
		
		buttons[0] = ((Button) this.findViewById(R.id.pink_button));
		buttons[1] = ((Button) this.findViewById(R.id.red_button));
		buttons[2] = ((Button) this.findViewById(R.id.blue_button));
		buttons[3] = ((Button) this.findViewById(R.id.yellow_button));
		buttons[4] = ((Button) this.findViewById(R.id.purple_button));
		buttons[5] = ((Button) this.findViewById(R.id.green_button));
		
		for (int i=0;i<buttons.length;i++) {
			buttons[i].setTag(i);
		}
	}
	
	/**
	 * este metodo se ejecuta al mostrar la interfaz y muestra las instrucciones
	 */
	@Override
	protected void onResume () {
		super.onResume();
		this.reset();
		
		// Se escribe el mensaje de Inicio
		String title = getString(R.string.instructions_label);
		String message = getString(R.string.instructions_desc);
		if (this.getIntent().getExtras() != null) {
			title	= getString(R.string.next_level_label);
			message = getString(R.string.level_label)+" "+level;
		}
		
		// Se muestra el Dialogo de Inicio
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(message).setTitle(title);
		builder.setPositiveButton(getString(R.string.start_label), new DialogInterface.OnClickListener() {
	           public void onClick(DialogInterface dialog, int id) {
	        	   display_colors();
	           }
	       });
		AlertDialog dialog = builder.create();
		dialog.show();
	}
	
	/**
	 * este metodo resetea las variables
	 */
	public void reset () {
		timer			 = new CTimer(1000, 300, this);
		answers			 = null;
		times_counter	 = 0;
		user_touch_times = 0;
	}
	
	/**
	 * este metodo se ejecuta cuando el usuario puede interactuar
	 */
	public void start_game () {
		runOnUiThread(new Runnable () {
			@Override
			public void run () {
				for (Button b: buttons) {
					b.setOnClickListener(self);
					b.setPressed(false);
					b.setText("");
				}
			}
		});
	}
	
	/**
	 * Este metodo empieza a mostrar los colores por orden
	 */
	public void display_colors () {
		this.reset();
		answers = new int[level];
		this.timer.start();
	}

	/**
	 * Este metodo lo llama el Timer cada 1 segundo hasta que se detenga
	 */
	@Override
	public void run_task(final boolean stop) {
		Log.i("LogToCheck_GameScreen", "looping "+times_counter);
		runOnUiThread(new Runnable () {
			@Override
			public void run () {
				for (Button b: buttons) {
					b.setPressed(false);
					b.setText("");
				}
				
				if (!stop) {
					int tmp_rand = rand.nextInt(5);
					if (tmp_rand == last) count_last++;
					else count_last = 1;
					
					Log.i("Testing::Selecting::", "Selected: "+tmp_rand);
					answers[times_counter] = (Integer) buttons[tmp_rand].getTag();
					buttons[tmp_rand].setPressed(true);
					buttons[tmp_rand].setText(""+count_last);
					
					last = tmp_rand;
					times_counter++;
					if (level <= times_counter) {
						timer.turnOff();
						times_counter = 0;
					}
				}
			}
		});
	}
	
	/**
	 * Este metodo se llama al apagar el timer
	 */
	@Override
	public void end_task() {
		start_game();
	}

	/**
	 * Comparar Los Clicks del Usuario con el random
	 */
	@Override
	public void onClick(View v) {
		Log.i("Testing::", "Click: "+v.getTag()+" == "+answers[user_touch_times]);
		if (((Integer) v.getTag()) == answers[user_touch_times]){
//			Toast.makeText(this, (user_touch_times+1)+" Bien", Toast.LENGTH_SHORT).show();
			user_touch_times++;
			
			if (user_touch_times >= answers.length) {
//				Toast.makeText(this, "Pasas al Nivel "+(level+1), Toast.LENGTH_LONG).show();
				this.reset();
				
				// se crea el intent
				Intent intent = new Intent(this, GameScreen.class);
				
				// se le mete el nivel en un diccionario
				Bundle data = new Bundle();
				data.putInt("level", level+1);
				
				// se le pasa el diccionario al intent y se cambia de vista
				intent.putExtras(data);
		        this.startActivity(intent);
			}
	        
		} else {
			user_touch_times = 0;
			user_errors++;
			if (user_errors >= 3) {
				Toast.makeText(this, R.string.you_loose_message, Toast.LENGTH_SHORT).show();
				
				prefs_readonly = this.getSharedPreferences("DroSimond", 0);
				Database db = new Database(this, "score_charts.sqlite", null, 1);
				db.new_value(prefs_readonly.getString("Username", "Anonimo"), level);
				
				Intent intent = new Intent(this, GameScores.class);
				this.startActivity(intent);
				
			} else {
				Toast.makeText(this, getString(R.string.bad_answer), Toast.LENGTH_SHORT).show();
			}
		} 
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)  {
	    if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
	    	prefs_readonly = this.getSharedPreferences("DroSimond", 0);
			Database db = new Database(this, "score_charts.sqlite", null, 1);
			db.new_value(prefs_readonly.getString("Username", "Anonimo"), level);
	    	
	    	Intent intent = new Intent(this, MainMenu.class);
			this.startActivity(intent);
			
	        return true;
	    }

	    return super.onKeyDown(keyCode, event);
	}
}
