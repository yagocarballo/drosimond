package com.karallo.drosimond.game;

public class CTimer extends Thread {
	private final CTimerTask timer_task;
	private final long wait;
	private final long start;
	
	private boolean stop = false;
	
	public CTimer (long wait, long start, CTimerTask timer_task) {
		this.wait = wait;
		this.start = start;
		this.timer_task = timer_task;
	}
	
	@Override
	public void run () {
		try {
			Thread.sleep(start);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		while (!this.stop) {
			try {
				Thread.sleep(wait);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			this.timer_task.run_task(stop);
		}
		
		this.timer_task.end_task();
	}

	public void turnOff() {
		this.stop = true;
	}
}
