package com.karallo.drosimond.database;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase.CursorFactory;

public abstract class SQLite extends SQLiteOpenHelper {
	private static String DB_PATH = "";
    private static String DB_NAME = "score_charts.sqlite";
    private SQLiteDatabase db; 
    private final Context contexto;
    
	public SQLite (Context contexto, String nombre, CursorFactory factory, int version) {
		super(contexto, nombre, factory, version);
		this.contexto = contexto;
		DB_PATH = contexto.getApplicationInfo().dataDir+"/databases/";
    }
	
	@Override
	public abstract void onCreate(SQLiteDatabase db);
	
	@Override
	public abstract void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion);
	
	/**
	 * Este m�todo crea la Base de Datos si no existe
	 */
	public void create_database () {
		boolean dbExist = checkDataBase();
    	if(dbExist){
    		//no hacer nada -- la base d datos ya existe
    	}else{
        	this.getReadableDatabase(); // Al llamar a esto se crear� una bd vacia
        	try {
    			this.copyDataBase();
    		} catch (IOException e) {
        		throw new Error("Error Copiando la Base de Datos");
        	}
    	}
	}
	
	/**
	 * Verifica si la Base de Datos Existe
	 * @return boolean (Existe o No Existe)
	 */
	private boolean checkDataBase(){
    	SQLiteDatabase checkDB = null;
    	try{
    		String path = DB_PATH + DB_NAME;
    		checkDB = SQLiteDatabase.openDatabase(path, null, SQLiteDatabase.OPEN_READONLY);
    	}catch(SQLiteException e){
    		// La Base de Datos No Existe
    	}
 
    	if(checkDB != null){
    		checkDB.close();
    	}
    	return checkDB != null ? true : false;
    }
	
	/**
	 * Este m�todo copia la base de datos de assets a la carpeta datos
	 * @throws IOException
	 */
	public void copyDataBase() throws IOException{
    	// Abrir la BD local como InputStream
    	InputStream myInput = this.contexto.getAssets().open(DB_NAME);
 
    	// Ruta a la BD vacia recien creada
    	String outFileName = DB_PATH + DB_NAME;
 
    	// Abrir BD Vacia como OutputStream
    	OutputStream myOutput = new FileOutputStream(outFileName);
 
    	// Transferir los bytes del inputstream al outputstream (mover d assets a carpeta datos)
    	byte[] buffer = new byte[1024];
    	int length;
    	while ((length = myInput.read(buffer))>0){
    		myOutput.write(buffer, 0, length);
    	}
 
    	// Se ciarran los Streams
    	myOutput.flush();
    	myOutput.close();
    	myInput.close();
    }
	
	/**
	 * M�todo para abrir la BD
	 * @throws SQLException
	 */
	public void openDataBase() throws SQLException{
        String path = DB_PATH + DB_NAME;
    	this.db = SQLiteDatabase.openDatabase(path, null, SQLiteDatabase.OPEN_READONLY);
    }
	
	/**
	 * Metodo para Cerrar la BD
	 */
    @Override
	public synchronized void close() {
    	    if(this.db != null)
    		    this.db.close();
    	    
    	    super.close();
	}

	public static String getDB_PATH() {
		return DB_PATH;
	}

	public static String getDB_NAME() {
		return DB_NAME;
	}
}
