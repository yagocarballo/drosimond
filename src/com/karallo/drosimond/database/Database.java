package com.karallo.drosimond.database;

import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;

public class Database extends SQLite{
    
	public Database(Context contexto, String nombre, CursorFactory factory, int version) {
		super(contexto, nombre, factory, version);
    }
	

	@Override
	public void onCreate(SQLiteDatabase db) {}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}
	
	/**
	 * Inserta una linea con el usuario y los puntos
	 * @param name String (nombre)
	 * @param level int (nivel)
	 */
	public void new_value (String name, int level){
		try {
			// Se crean los Valores
			ContentValues values = new ContentValues();
	        values.put("nombre", name);
	        values.put("nivel", level);
	        
	        // Se abre la bd y se insertan los datos
	        super.create_database();
			super.openDataBase();
			super.getWritableDatabase().insert("puntuaciones", null, values);
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			super.close();
		}
	}
	
	/**
	 * Limpia la tabla
	 */
	public void clean (){
		try {
	        File db_file = new File(super.getDB_PATH()+super.getDB_NAME());
	        db_file.delete();
	        
	        super.create_database();
			super.openDataBase();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			super.close();
		}
	}
	
	/**
	 * Se descarga la Tabla de Puntuaciones
	 * @return ArrayList<Point> (ArrayList con los Puntos)
	 */
	public ArrayList<Point> score_chart () {
		ArrayList<Point> points = new ArrayList<Point>(5);
		
		try {
			super.create_database();
			super.openDataBase();
			Cursor cursorQuery = super.getReadableDatabase().query(
			        "puntuaciones",         // Nombre de la tabla
			        new String[]{ 
			        		"nombre", 
			        		"nivel"
			        },		                // columnas
			        null,                   // Columnas para el where
			        null,                   // Values para el where 
			        null,                   // clausula group by
			        null,                   // filtro para group by 
			        null);                  // orden
			cursorQuery.moveToFirst();
			
			while(cursorQuery.moveToNext()) {
				points.add(new Point(
			    		cursorQuery.getString(cursorQuery.getColumnIndexOrThrow("nombre")),
			    		cursorQuery.getInt(cursorQuery.getColumnIndexOrThrow("nivel"))
			    ));
			}
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return points;
	}
	
	
}
