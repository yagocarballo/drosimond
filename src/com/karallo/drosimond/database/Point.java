package com.karallo.drosimond.database;

public class Point {
	private String name = "Anonimo";
	private int level	= 1;
	
	public Point () {}
	
	public Point (String name, int level) {
		this.name	= name;
		this.level	= level;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}
}
