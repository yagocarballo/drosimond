package com.karallo.drosimond.scores;

import java.util.ArrayList;

import com.karallo.drosimond.database.Point;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ShowScoresAdapter extends ArrayAdapter<Point> {
	private ArrayList<Point> points;
	private LayoutInflater inflater;

	public ShowScoresAdapter(Context context, int textViewResourceId, ArrayList<Point> points) {
		super(context, textViewResourceId, points);
		this.points = points;
	}
	
	public void setInflater (LayoutInflater inflater) {
		this.inflater = inflater;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;
		if (v == null) {
			LayoutInflater vi = this.inflater;
			v = vi.inflate(com.karallo.drosimond.R.layout.activity_show_scores, null);
		}
		
		Point point = points.get(position);
		if (point != null) {
			TextView name = (TextView) v.findViewById(com.karallo.drosimond.R.id.name_label);
			TextView level = (TextView) v.findViewById(com.karallo.drosimond.R.id.level_label);

			if (name != null) {
				name.setText(point.getName());
			}

			if(level != null) {
				level.setText(""+point.getLevel());
			}
		}
		return v;
	}
}
