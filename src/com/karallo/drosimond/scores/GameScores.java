package com.karallo.drosimond.scores;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;

import com.karallo.drosimond.MainMenu;
import com.karallo.drosimond.R;
import com.karallo.drosimond.database.Database;

public class GameScores extends Activity implements OnClickListener {
	private Database db;
	private Button clean_button;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_game_scores);
		
		db = new Database(this, "score_charts.sqlite", null, 1);
		
		ShowScoresAdapter scoresAdapter = new ShowScoresAdapter(this, R.layout.activity_show_scores, db.score_chart());
		scoresAdapter.setInflater((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE));
		
		ListView listView = (ListView) findViewById(R.id.scores_list);
        listView.setAdapter(scoresAdapter);
        
        // boton de limpiar
        clean_button = (Button) findViewById(R.id.clean_button);
        clean_button.setOnClickListener(this);
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)  {
	    if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
	    	Intent intent = new Intent(this, MainMenu.class);
			this.startActivity(intent);
			
	        return true;
	    }

	    return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onClick(View v) {
		if (clean_button == v) {
			db = new Database(this, "score_charts.sqlite", null, 1);
			db.clean();
			
			ShowScoresAdapter scoresAdapter = new ShowScoresAdapter(this, R.layout.activity_show_scores, db.score_chart());
			scoresAdapter.setInflater((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE));
			
			ListView listView = (ListView) findViewById(R.id.scores_list);
	        listView.setAdapter(scoresAdapter);
		}
	}
}
