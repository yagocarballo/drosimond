package com.karallo.drosimond;

import com.karallo.drosimond.R;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class NewUser extends Activity implements OnClickListener {
	private SharedPreferences prefs_readonly = null;
	private EditText username_input = null;
	private Button next_button = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_new_user);
		
		prefs_readonly = this.getSharedPreferences("DroSimond", 0);
		
		username_input	= ((EditText) this.findViewById(R.id.username_input));
		next_button		= ((Button) this.findViewById(R.id.next_button));
		
		this.next_button.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		String username = ""+username_input.getText();
		if (username.equals("")) {
			Toast.makeText(this, "El Campo de Nombre de Usuario no puede estar Vacio.", Toast.LENGTH_LONG).show();
		} else {
			SharedPreferences.Editor prefs = prefs_readonly.edit();
			prefs.putString("Username", username);
			prefs.commit();
			
			Intent intent = new Intent(this, MainMenu.class);
	        this.startActivity(intent);
		}
	}
}
